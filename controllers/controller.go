package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/renegatumsoulteame/auth_service/db"
	"bitbucket.org/renegatumsoulteame/parking_context/json"
	"bitbucket.org/renegatumsoulteame/parking_structs/entitys"
)

func Login(w http.ResponseWriter, r *http.Request) {
	res := LoginReq{}
	err := json.GetJsonOrBadRequest(&res, r, w)
	if err != nil {
		fmt.Println("eror cast")
	}
	if res == (LoginReq{}) {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Empty struct or fields!"))
	}
	uuid, err := entitys.AuthUser(db.CocrouchDBSession, res.Login, res.Code)
	js, err := json.ToJson(uuid)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("not uuid"))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write(js)
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	res := entitys.User{}
	err := json.GetJsonOrBadRequest(&res, r, w)
	if err != nil {
		fmt.Println("eror cast")
	}
	res = entitys.GetUserByUUID(db.CocrouchDBSession, res.Token)
	res.ShortPass = ""
	res.Update(db.CocrouchDBSession)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}

func Registration(w http.ResponseWriter, r *http.Request) {
	res := entitys.User{}
	err := json.GetJsonOrBadRequest(&res, r, w)
	if err != nil {
		fmt.Println("eror cast")
	}
	res.Update(db.CocrouchDBSession)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}

func Profile(w http.ResponseWriter, r *http.Request) {}
