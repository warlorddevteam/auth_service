package controllers

type LoginReq struct {
	Login string `json:"login"`
	Code string  `json:"code"`
}