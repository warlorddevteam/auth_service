package conf

import (
	contextconf "bitbucket.org/renegatumsoulteame/parking_context/conf"
    "bitbucket.org/renegatumsoulteame/parking_structs/entitys"
)

var InitConfig entitys.Conf

func init() {
	InitConfig = entitys.Conf{AppName: "auth", AppMod: "test"}
	contextconf.GetConfig(&InitConfig, InitConfig.AppName, InitConfig.AppMod)
}
