package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/renegatumsoulteame/auth_service/conf"
	_ "bitbucket.org/renegatumsoulteame/auth_service/router"
	"github.com/gorilla/handlers"
)

func main() {
	log.Println("ok runing on port:", conf.InitConfig.HTTPServerPort)
	log.Fatal(http.ListenAndServe(conf.InitConfig.ConnectionString(),
	handlers.LoggingHandler(os.Stdout, http.DefaultServeMux)))
}
