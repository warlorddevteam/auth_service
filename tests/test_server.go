package tests

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/renegatumsoulteame/auth_service/conf"
	"github.com/gorilla/handlers"
)

//RunTestServer - run the test server
func RunTestServer() {
	log.Println("ok runing on port:", conf.InitConfig.HTTPServerPort)
	log.Fatal(http.ListenAndServe(conf.InitConfig.ConnectionString(),
		handlers.LoggingHandler(os.Stdout, http.DefaultServeMux)))
}
