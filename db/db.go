package db

import (
	//"github.com/mediocregopher/radix.v2/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"bitbucket.org/renegatumsoulteame/auth_service/conf"
)


var CocrouchDBSession *gorm.DB
//var RedisSession *redis.Client

func init() {
	 // RedisSession = conf.InitConfig.RedisConnection()
	CocrouchDBSession = conf.InitConfig.CocrouchDBSesion()
}
